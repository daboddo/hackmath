defmodule HackmathWeb.PageController do
  use HackmathWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
